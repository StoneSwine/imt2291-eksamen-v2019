# imt2291 eksamen v2019

For oppgave 1 så skriver du inn svaret direkte under oppgaveteksten, for de andre oppgavene så angir dere under oppgaveteksten hvilke filer som inneholder koden for den enkelte oppgaven.
En del av kommentarene underveis i koden er både på norsk og engelsk. 


## Oppgave 1

Beskriv hva som skjer når du kjører "codecept run" i test containeren på docker oppsettet i katalogen oppgave1. Hva kjøres i hvilke containere, hvilke nettverkskall gjøres fra hvilke containere til hvilke containere og i hvilken rekkefølge.
### Diverse
For å få oppgave1 til å fungere var jeg nødt til å lage katalogen "test/tests/unit/". Jeg antar at det er tenkt at codecept run skal kjøres når nettsiden fungerer. 
Når jeg besøker nettsiden får jeg feilmeldingen: `TypeError: NetworkError when attempting to fetch resource.` Testene passerer, så jeg ignorerer denne feilmeldingen.

### Svar
Når `codecept run` kjøres i test dockeren så kjører den acceptance testene siden det ikke finnes noen unit tester. 

I konfigurasjonsfilen `acceptance.suite.yml` beskrives det hva URL'en til webserveren er og instillinger for selenium. Nettsiden i oppgave1 inneholder javascript, man er derfor nødt til å ha en nettleser for å kunne kjøre koden og se nettsiden. Dette gjøres via en selenium-node (chrome) som igjen er koblet opp imot selenium-hub. Selenium-hub fungerer som en slags ruter og videresender kommandoer til riktig docker(nettleser) angitt i konfigurasjonsfilen.

Testen, `FirstCest.php` som kjøres i test-dockeren beskriver alle kommandoene som skal kjøres via nettleser-dockeren (chrome). Den helhetlige flyten er noe likende dette:
```
Testdocker --> selenium-hub --> selnium-node-chrome --> www --> selnium-node-chrome --> selenium-hub --> Testdocker
```

Testene beskriver hva nettleseren skal gjøre. Den første linjen sier hva adressen til nettsiden skal være. Deretter vil testene passere dersom teksten beskrevet i testen finnes på nettsiden. Til slutt tar nettleseren et skjermbilde i storskjerm, og sender det tilbake til test-dockeren via selenium-hub og lagrer det i jkatalogen  `test/tests/_output/debug`.

I tillegg til trafikk mellom "nettsiden" og testoppsettet vil det også være trafikk internt mellom `php`, `www` og `db` -dockerne. Når selenium-noden(nettleseren) sender en "GET" forespørsel til www-dockeren. så vil polymer sørge før at rendret javascript kode kommer til klienten. Fra klienten blir det gjort diverse kall til php-dockeren (backend) som igjen gjør kall til db-dockeren. Resultatet fra database-forespørselen blir sendt tilbake i json-format fra backend til frontend(klienten) og vist i nettleseren. 



## Oppgave 2-4

Til oppgave 2-4 har jeg valgt å bruke mitt eget docker oppsett. Bruk `docker-compose up --build` for å bygge dockerene. 
**NB**
Besvarelsene er tilgjengelig fra `http://localhost:8081/`, da jeg bruker mitt eget oppsett. 

## Oppgave 2 (PHP)

Lag en side som presenterer en form for å lage en ny bruker (uten avatar bilde.) Dvs, brukeren skal skrive inn brukernavn (bruk e-post adresse som brukernavn), passord, fornavn og etternavn. Sjekk for gyldige data på både klient og server (dvs, gyldig e-post adresse, minimum lengde på passord, fornavn og etternavn.) La samme side (skript) ta i mot data som den siden som presenterer formen, lagre ny bruker i databasen og gi fornuftige tilbakemeldinger ved suksess/feil.
Bruk twig for å presentere data.

### Svar
Koden til oppgave 2 ligger i følgende katalog: `imt2291-eksamen-v2019/oppgave2-4/www/02` og er tilgjengelig via `http://localhost:8081/02/oppgave2.php`

Jeg antar at et fornavn og etternavn  er minst tre tegn langt.

## Oppgave 3 (JavaScript)

NB, det finnes ferdige PHP skript i *oppgave2-4/www/api* for å hente/oppdatere informasjon.

Lag en side som lister ut alle brukere (med avatar bilde dersom det finnes, avatar bildet hentes fra *api/avatar.php*), hent informasjon om brukerne med fetch (i JavaScript, hentes fra *api/fetchUsers.php*). Dvs at informasjon om brukerne ikke finnes på siden når den siden hentes fra serveren. Presenter listen på venstre del av skjermen. Når en velger en bruker fra listen skal en kunne redigere informasjon om brukeren på høyre side av skjermen.

All informasjon om brukeren skal kunne redigeres med unntak av autogenerert ID i databasen. Send informasjon om brukernavn/fornavn/etternavn/passord til serveren når brukeren trykker en knapp (bruk fetch med metoden POST, sendes til *api/updateUser.php*). NB, for å oppdatere fornavn/etternavn trenger en ikke oppgi gammelt passord, men for å oppdatere brukernavn og eller passord så må det gamle passordet oppgis. Se *api/updateUser.php* for detaljer.

Det skal også være mulig å laste opp avatar bilde av brukeren. Opplasting av bilder *skal* gjøres med en progressbar (start opplastingen så snart brukeren har valgt et bilde, sendes til *api/updateAvatar.php*).

### Svar:
Jeg var nødt til å modifisere `/www/classes/DB.php` til å passe til mitt dockeroppsett.
Koden til oppgave 3 ligger i følgende katalog: `imt2291-eksamen-v2019/oppgave2-4/www/03` og er tilgjengelig via `http://localhost:8081/03/oppgave3.html`
Ble ikke helt ferdig med oppgave 3

## Oppgave 4 (Web komponenter)

Lag en kopi av koden fra oppgave 3 (eller om du ikke fikk til oppgave 3, lag en enkel liste.) Du skal nå lage en web komponent (valgfritt om du bruker lit-element eller polymer-element) for å vise informasjonen for en og en bruker i listen til venstre. Dvs at innholdet i hvert enkelt element i listen skal være LI elementer som hver inneholder den nye taggen (web komponenten) som du lager. Du velger selv hvordan informasjon om brukeren overføres til komponenten. Beskriv de endringene du må gjøre i koden fra oppgave tre for at dette fortsatt skal fungere (hvordan finner du ID til den brukeren det trykkes på.)

NB, i *oppgave2-4/www/node_modules* ligger både lit-element og polymer-element med de endringer som skal til for at de fungerer uten å bruke "polymer serve" (dvs, koden fungerer direkte i docker/XAMPP).

### Svar
Koden til oppgave 4 ligger i følgende katalog: `imt2291-eksamen-v2019/oppgave2-4/www/04`
Oppgave 4 fungerer ikke. Fikk bare startet på den, men ble ikke ferdig.