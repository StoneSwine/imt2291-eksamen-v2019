window.onload = function () {
    fetch("/api/fetchUsers.php", {
        method: "GET",
        credentials: "include"
    }).then(res => res.json()).then(res => {
        res.forEach(makeList)
    })
};
 
/**
 * Parse listen med alle brukerne
 * @param element
 */
function makeList(element) {
    console.log(element);
    var item = document.createElement('div');
    var leftside = document.getElementsByClassName("left")[0];

    html = `ID: ${element.uid}  Brukernavn: ${element.uname}  Fornavn:  ${element.firstName}  Etternavn:  ${element.lastName} `;


    //check if the element has an avatar
    if (element.hasAvatar !== "0") {
        fetch(`/api/avatar.php?id=${element.uid}`, {
            method: "GET",
        }).then(res => {
            html += `<br> ${res}`;
            item.appendChild(document.createTextNode(res));
        })
    }

    item.innerHTML = html;

    /*Imporert kode start: https://www.w3schools.com/jsref/met_document_createattribute.asp*/
    var idatrribute = document.createAttribute("data-uid");
    idatrribute.value = element.uid;
    item.setAttributeNode(idatrribute);
    /*Imporert kode slutt*/

//click-listener for hvert element
    leftside.appendChild(item);
    item.addEventListener("click", function (event) {
        editUser(event.target.getAttribute("data-uid"));
    });
}

/**
 * Redigere en brukers data
 * @param uid
 */
function editUser(uid) {
    //get the right side of the screen
    var right = document.getElementsByClassName('right')[0];

    //Delete the previous form if it exists
    if (document.contains(document.getElementsByTagName("form")[0])) {
        right.removeChild(document.getElementsByTagName("form")[0]);
    }

    //Koden er inspirert fra: https://stackoverflow.com/questions/6964927/how-to-create-a-form-dynamically-via-javascript
    fetch(`/api/fetchUser.php?id=${uid}`, {
        method: "GET",
        credentials: "include"
    }).then(res => res.json()).then(res => {

        console.log(res);
        //Lage en form og hente data fra bruker som blir value i input feltene.
        var f = document.createElement("form");
        f.setAttribute('method', "post");
        f.setAttribute('action', "/api/updateUser.php");

        //brukernavn
        var uname = document.createElement("input");
        uname.setAttribute('type', "text");
        uname.setAttribute('name', "uname");
        uname.setAttribute('value', res.uname);

        //fornavn
        var fname = document.createElement("input");
        fname.setAttribute('type', "text");
        fname.setAttribute('name', "firstName");
        fname.setAttribute('value', res.firstName);

        //etternavn
        var lname = document.createElement("input");
        lname.setAttribute('type', "text");
        lname.setAttribute('name', "lastName");
        lname.setAttribute('value', res.lastName);

        //passord
        var newpass = document.createElement("input");
        newpass.setAttribute("type", "password");
        newpass.setAttribute("name", "pwd");

        //gammelt password som skal vises frem etterhvert som de feltene som krever det endres
        var oldpass = document.createElement("input");
        oldpass.setAttribute("type", "hidden");
        oldpass.setAttribute("name", "oldpwd");

        //sende med bruker id
        var uid = document.createElement("input");
        uid.setAttribute("type", "hidden");
        uid.setAttribute("name", "uid");
        uid.setAttribute('value', res.uid);

        var btn = document.createElement("button");
        btn.innerHTML = "Submit";

        f.innerHTML += `${uname.outerHTML}<br>${fname.outerHTML}<br>${lname.outerHTML}<br>${newpass.outerHTML}<br>${oldpass.outerHTML}<br>${uid.outerHTML}<br>${btn.outerHTML}`;

        right.appendChild(f);

        //Hvis noe som krever passord redigeres --> lage et passordfelt som er required --> rakk ikke
    });
}