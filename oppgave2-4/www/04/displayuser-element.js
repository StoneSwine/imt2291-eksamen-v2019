import { LitElement, html, css } from 'lit-element';
import "./displayuser-element.js";

class DisplayuserElement  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        uid: {
            type: String,
        },
        uname: {
            type: String,
        },
        fname: {
            type: String,
        },
        lname: {
            type: String,
        }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      ID: ${this.uid} Brukernavn: ${this.uname} Fornavn: ${this.fname} Etternavn: ${this.lname} 
    `;
  }
}

customElements.define('displayuser-element', DisplayuserElement);