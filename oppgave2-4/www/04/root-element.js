import {
  LitElement,
  html,
  css
} from "../node_modules/lit-element/lit-element.js";

class RootElement extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      users: {
        type: Array
      }
    };
  }

  constructor() {
    super();
    this.users = [];
  }

  /**
   * Use for one-time configuration of your component after local DOM is
   * initialized.
   */
  ready() {
    super.ready();
    
    fetch("localhost:8081/api/fetchUsers.php", {
      method: "GET",
      credentials: "include"
    }).then(res=>res.json())
      .then(res => {
        res.forEach(this.makeList);
      });
  }

  render() {
    return html`
      ${this.users.map(
        user => html`
          <displayuser-element
            uid="${user.uid}"
            uname="${user.uname}"
            fname="${user.firstName}"
            lname="${user.lastName}"
          >
          </displayuser-element>
        `
      )}
    `;
  }

  makeList(element) {
    this.users.push(element);
  }
}

customElements.define("root-element", RootElement);
