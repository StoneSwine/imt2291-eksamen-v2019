<?php

//Hent twig fra context til dette scriptet
require realpath(dirname(__FILE__)) . '/../../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__)) . "/");
$twig = new Twig_Environment($loader, array(//    'cache' => './compilation_cache',
));

//Twig meldinger
$data = array(
    "message" => "",
);

//bool for å validere data
$allcorrect = true;

//Formen er sendt in
if (isset($_POST['subform'])) {
    //Hent data fra form
    $credentials = array(
        'username' => $_POST['formuname'],
        'password' => $_POST['formpass'],
        'fname' => $_POST['formfname'],
        'lname' => $_POST['formlname']
    );

    //database tilkobling
    $db = new PDO('mysql:host=database;dbname=myDb;port=3306', "root", "password99");

    //Validere data: (epost er riktig format og lengde på  stringer)
    //Epost-sjekk er hentet fra https://stackoverflow.com/questions/5855811/how-to-validate-an-email-in-php
    if (!filter_var($credentials['username'], FILTER_VALIDATE_EMAIL)) {
        $data['message'] = "brukernavn må være en epost";
        $allcorrect = false;

    }
    if (strlen($credentials['password']) < 6) {
        $data['message'] = "passord må være over seks tegn langt";
        $allcorrect = false;

    }
    if ((strlen($credentials['fname']) < 3) && (strlen($credentials['lame']) < 3)) {
        $data['message'] = "Fornavn og etternavn må være over tre tegn langt";
        $allcorrect = false;

    }

    //Alt av data har rett format og brukeren kan opprettes
    if($allcorrect) {
        //sql statement
        $sql = "INSERT INTO `myDb`.`user` (`uname`, `pwd`, `firstName`, `lastName`, `avatar`) VALUES (:username, :pass, :firstname, :lastname, null)";
        $sth = $db->prepare($sql);
        //tilegne variabler for å forhindre sql injection
        $sth->bindParam(":username", $credentials['username']);
        //hashe passord
        $sth->bindParam(":pass", password_hash($credentials['password'], PASSWORD_DEFAULT));
        $sth->bindParam(":firstname", $credentials['fname']);
        $sth->bindParam(":lastname", $credentials['lname']);
        //Brukeren kunne oprettes
        if ($sth->execute()) {
            $data['message'] = "brukeren ble oprettet suksessfult";
        } else {
            $data['message'] = "Det skjedde en feil. Prøv på nytt";
        }
    }
}

//vis siden, med eventuelle meldinger
echo $twig->render("form.twig", array("data" => $data));
